#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"
#include "library.c"
extern JNIEnv *javaEnv;

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

JNIEXPORT jint JNICALL Java_library_factorial_1
  (JNIEnv *env, jobject object, jint n)
{
    javaEnv = env;
    int c_n = toInt(n);
    int c_outValue = factorial(c_n);
    return toJint(c_outValue);
}
