/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#ifndef _jni_inout_h
#define _jni_inout_h

#include <stdlib.h>
#include <stdbool.h>
#include "jni.h"


////////////////////////////////////////////////////
//                     Integer                    //
////////////////////////////////////////////////////
jobject newIntegerObject
  (int* value);
void setIntegerValue
  (jobject object, int* value);
int getIntegerValue
  (jobject object);
int* intPtr
  (jobject object);
void setIntPtrValue
  (int* intPtr, int value);
int getIntPtrValue
  (int* intPtr);
int** toIntMatrixPtr
  (int* matrix, int rows, int columns);
int* toIntMatrixRegionPtr
  (int** matrix, int rows, int columns);
void setIntArray
  (int* outArray, jintArray arrayValue, jint arrayLength);
void setIntMatrix
  (int** inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
void setIntMatrixRegion
  (int* inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
int toInt
  (jint value);
int* toIntArray
  (jintArray arrayValue, jint arrayLength);
int** toIntMatrix
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
int* toIntMatrixRegion
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
void setJintArray
  (jintArray outArray, int* arrayValue, int* arrayLength);
void setJintMatrix
  (jobjectArray matrix, int** matrixValue, int* matrixRows, int* matrixColumns);
void setJintMatrixRegion
  (jobjectArray matrix, int* matrixValue, int* matrixRows, int* matrixColumns);
jint toJint
  (int value);
jintArray toJintArray
  (int* arrayValue, int* arrayLength);
jobjectArray toJintMatrix
  (int** matrixValue, int* matrixRows, int* matrixColumns);
jobjectArray toJintMatrixRegion
  (int* matrixValue, int* matrixRows, int* matrixColumns);


////////////////////////////////////////////////////
//                      Byte                      //
////////////////////////////////////////////////////

typedef signed char byte;

jobject newByteObject
  (byte* value);
void setByteValue
  (jobject object, byte* value);
byte getByteValue
  (jobject object);
byte* bytePtr
  (jobject object);
void setBytePtrValue
  (byte* bytePtr, byte value);
byte getBytePtrValue
  (byte* bytePtr);
byte** toByteMatrixPtr
  (byte* matrix, int rows, int columns);
byte* toByteMatrixRegionPtr
  (byte** matrix, int rows, int columns);
void setByteArray
  (byte* outArray, jbyteArray arrayValue, jint arrayLength);
void setByteMatrix
  (byte** inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
void setByteMatrixRegion
  (byte* inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
byte toByte
  (jbyte value);
byte* toByteArray
  (jbyteArray arrayValue, jint arrayLength);
byte** toByteMatrix
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
byte* toByteMatrixRegion
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
void setJbyteArray
  (jbyteArray outArray, byte* arrayValue, int* arrayLength);
void setJbyteMatrix
  (jobjectArray matrix, byte** matrixValue, int* matrixRows, int* matrixColumns);
void setJbyteMatrixRegion
  (jobjectArray matrix, byte* matrixValue, int* matrixRows, int* matrixColumns);
jbyte toJbyte
  (byte value);
jbyteArray toJbyteArray
  (byte* arrayValue, int* arrayLength);
jobjectArray toJbyteMatrix
  (byte** matrixValue, int* matrixRows, int* matrixColumns);
jobjectArray toJbyteMatrixRegion
  (byte* matrixValue, int* matrixRows, int* matrixColumns);



////////////////////////////////////////////////////
//                     Short                      //
////////////////////////////////////////////////////
jobject newShortObject
  (short* value);
void setShortValue
  (jobject object, short* value);
short getShortValue
  (jobject object);
short* shortPtr
  (jobject object);
void setShortPtrValue
  (short* shortPtr, short value);
short getShortPtrValue
  (short* shortPtr);
short** toShortMatrixPtr
  (short* matrix, int rows, int columns);
short* toShortMatrixRegionPtr
  (short** matrix, int rows, int columns);
void setShortArray
  (short* outArray, jshortArray arrayValue, jint arrayLength);
void setShortMatrix
  (short** inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
void setShortMatrixRegion
  (short* inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
short toShort
  (jshort value);
short* toShortArray
  (jshortArray arrayValue, jint arrayLength);
short** toShortMatrix
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
short* toShortMatrixRegion
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
void setJshortArray
  (jshortArray outArray, short* arrayValue, int* arrayLength);
void setJshortMatrix
  (jobjectArray matrix, short** matrixValue, int* matrixRows, int* matrixColumns);
void setJshortMatrixRegion
  (jobjectArray matrix, short* matrixValue, int* matrixRows, int* matrixColumns);
jshort toJshort
  (short value);
jshortArray toJshortArray
  (short* arrayValue, int* arrayLength);
jobjectArray toJshortMatrix
  (short** matrixValue, int* matrixRows, int* matrixColumns);
jobjectArray toJshortMatrixRegion
  (short* matrixValue, int* matrixRows, int* matrixColumns);


////////////////////////////////////////////////////
//                      Long                      //
////////////////////////////////////////////////////
jobject newLongObject
  (long* value);
void setLongValue
  (jobject object, long* value);
long getLongValue
  (jobject object);
long* longPtr
  (jobject object);
void setLongPtrValue
  (long* longPtr, long value);
long getLongPtrValue
  (long* longPtr);
long** toLongMatrixPtr
  (long* matrix, int rows, int columns);
long* toLongMatrixRegionPtr
  (long** matrix, int rows, int columns);
void setLongArray
  (long* outArray, jlongArray arrayValue, jint arrayLength);
void setLongMatrix
  (long** inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
void setLongMatrixRegion
  (long* inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
long toLong
  (jlong value);
long* toLongArray
  (jlongArray arrayValue, jint arrayLength);
long** toLongMatrix
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
long* toLongMatrixRegion
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
void setJlongArray
  (jlongArray outArray, long* arrayValue, int* arrayLength);
void setJlongMatrix
  (jobjectArray matrix, long** matrixValue, int* matrixRows, int* matrixColumns);
void setJlongMatrixRegion
  (jobjectArray matrix, long* matrixValue, int* matrixRows, int* matrixColumns);
jlong toJlong
  (long value);
jlongArray toJlongArray
  (long* arrayValue, int* arrayLength);
jobjectArray toJlongMatrix
  (long** matrixValue, int* matrixRows, int* matrixColumns);
jobjectArray toJlongMatrixRegion
  (long* matrixValue, int* matrixRows, int* matrixColumns);


////////////////////////////////////////////////////
//                      Float                     //
////////////////////////////////////////////////////
jobject newFloatObject
  (float* value);
void setFloatValue
  (jobject object, float* value);
float getFloatValue
  (jobject object);
float* floatPtr
  (jobject object);
void setFloatPtrValue
  (float* floatPtr, float value);
float getFloatPtrValue
  (float* floatPtr);
float** toFloatMatrixPtr
  (float* matrix, int rows, int columns);
float* toFloatMatrixRegionPtr
  (float** matrix, int rows, int columns);
void setFloatArray
  (float* outArray, jfloatArray arrayValue, jint arrayLength);
void setFloatMatrix
  (float** inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
void setFloatMatrixRegion
  (float* inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
float toFloat
  (jfloat value);
float* toFloatArray
  (jfloatArray arrayValue, jint arrayLength);
float** toFloatMatrix
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
float* toFloatMatrixRegion
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
void setJfloatArray
  (jfloatArray outArray, float* arrayValue, int* arrayLength);
void setJfloatMatrix
  (jobjectArray matrix, float** matrixValue, int* matrixRows, int* matrixColumns);
void setJfloatMatrixRegion
  (jobjectArray matrix, float* matrixValue, int* matrixRows, int* matrixColumns);
jfloat toJfloat
  (float value);
jfloatArray toJfloatArray
  (float* arrayValue, int* arrayLength);
jobjectArray toJfloatMatrix
  (float** matrixValue, int* matrixRows, int* matrixColumns);
jobjectArray toJfloatMatrixRegion
  (float* matrixValue, int* matrixRows, int* matrixColumns);


////////////////////////////////////////////////////
//                     Double                     //
////////////////////////////////////////////////////
jobject newDoubleObject
  (double* value);
void setDoubleValue
  (jobject object, double* value);
double getDoubleValue
  (jobject object);
double* doublePtr
  (jobject object);
void setDoublePtrValue
  (double* doublePtr, double value);
double getDoublePtrValue
  (double* doublePtr);
double** toDoubleMatrixPtr
  (double* matrix, int rows, int columns);
double* toDoubleMatrixRegionPtr
  (double** matrix, int rows, int columns);
void setDoubleArray
  (double* outArray, jdoubleArray arrayValue, jint arrayLength);
void setDoubleMatrix
  (double** inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
void setDoubleMatrixRegion
  (double* inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
double toDouble
  (jdouble value);
double* toDoubleArray
  (jdoubleArray arrayValue, jint arrayLength);
double** toDoubleMatrix
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
double* toDoubleMatrixRegion
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
void setJdoubleArray
  (jdoubleArray outArray, double* arrayValue, int* arrayLength);
void setJdoubleMatrix
  (jobjectArray matrix, double** matrixValue, int* matrixRows, int* matrixColumns);
void setJdoubleMatrixRegion
  (jobjectArray matrix, double* matrixValue, int* matrixRows, int* matrixColumns);
jdouble toJdouble
  (double value);
jdoubleArray toJdoubleArray
  (double* arrayValue, int* arrayLength);
jobjectArray toJdoubleMatrix
  (double** matrixValue, int* matrixRows, int* matrixColumns);
jobjectArray toJdoubleMatrixRegion
  (double* matrixValue, int* matrixRows, int* matrixColumns);


////////////////////////////////////////////////////
//                     Boolean                    //
////////////////////////////////////////////////////
jobject newBooleanObject
  (bool* value);
void setBooleanValue
  (jobject object, bool* value);
bool getBooleanValue
  (jobject object);
bool* boolPtr
  (jobject object);
void setBoolPtrValue
  (bool* boolPtr, bool value);
bool getboolPtrValue
  (bool* boolPtr);
bool** toBoolMatrixPtr
  (bool* matrix, int rows, int columns);
bool* toBoolMatrixRegionPtr
  (bool** matrix, int rows, int columns);
void setBoolArray
  (bool* outArray, jbooleanArray arrayValue, jint arrayLength);
void setBoolMatrix
  (bool** inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
void setBoolMatrixRegion
  (bool* inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
bool toBool
  (jboolean value);
bool* toBoolArray
  (jbooleanArray arrayValue, jint arrayLength);
bool** toBoolMatrix
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
bool* toBoolMatrixRegion
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
void setJboolArray
  (jbooleanArray outArray, bool* arrayValue, int* arrayLength);
void setJboolMatrix
  (jobjectArray matrix, bool** matrixValue, int* matrixRows, int* matrixColumns);
void setJboolMatrixRegion
  (jobjectArray matrix, bool* matrixValue, int* matrixRows, int* matrixColumns);
jboolean toJboolean
  (bool value);
jbooleanArray toJbooleanArray
  (bool* arrayValue, int* arrayLength);
jobjectArray toJbooleanMatrix
  (bool** matrixValue, int* matrixRows, int* matrixColumns);
jobjectArray toJbooleanMatrixRegion
  (bool* matrixValue, int* matrixRows, int* matrixColumns);


////////////////////////////////////////////////////
//                    Character                   //
////////////////////////////////////////////////////
jobject newCharacterObject
  (char* value);
void setCharacterValue
  (jobject object, char* value);
char getCharacterValue
  (jobject object);
char* charPtr
  (jobject object);
void setCharPtrValue
  (char* charPtr, char value);
char getCharPtrValue
  (char* charPtr);
char** toCharMatrixPtr
  (char* matrix, int rows, int columns);
char* toCharMatrixRegionPtr
  (char** matrix, int rows, int columns);
void setCharArray
  (char* outArray, jcharArray arrayValue, jint arrayLength);
void setCharMatrix
  (char** inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
void setCharMatrixRegion
  (char* inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
char toChar
  (jchar value);
char* toCharArray
  (jcharArray arrayValue, jint arrayLength);
char** toCharMatrix
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
char* toCharMatrixRegion
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
void setJcharArray
  (jcharArray outArray, char* arrayValue, int* arrayLength);
void setJcharMatrix
  (jobjectArray matrix, char** matrixValue, int* matrixRows, int* matrixColumns);
void setJcharMatrixRegion
  (jobjectArray matrix, char* matrixValue, int* matrixRows, int* matrixColumns);
jchar toJchar
  (char value);
jcharArray toJcharArray
  (char* arrayValue, int* arrayLength);
jobjectArray toJcharMatrix
  (char** matrixValue, int* matrixRows, int* matrixColumns);
jobjectArray toJcharMatrixRegion
  (char* matrixValue, int* matrixRows, int* matrixColumns);


////////////////////////////////////////////////////
//                     String                     //
////////////////////////////////////////////////////
jobject newStringObject
  (const char** value);
void setStringValue
  (jobject object, const char** value);
const char* getStringValue
  (jobject object);
const char** StringPtr
  (jobject object);
void setStringPtrValue
  (const char** charPtr, const char* value);
const char* getStringPtrValue
  (const char** charPtr);
const char*** toStringMatrixPtr
  (const char** matrix, int rows, int columns);
const char** toStringMatrixRegionPtr
  (const char*** matrix, int rows, int columns);
void setStringArray
  (const char** inArrayValue, jobjectArray arrayValue, jint arrayLength);
void setStringMatrix
  (const char*** inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
void setStringMatrixRegion
  (const char** inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
const char* toString
  (jstring value);
const char** toStringArray
  (jobjectArray arrayValue, jint arrayLength);
const char*** toStringMatrix
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
const char** toStringMatrixRegion
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns);
void setJstringArray
  (jobjectArray array, const char** arrayValue, int* arrayLength);
void setJstringMatrix
  (jobjectArray matrix, const char*** matrixValue, int* matrixRows, int* matrixColumns);
void setJstringMatrixRegion
  (jobjectArray matrix, const char** matrixValue, int* matrixRows, int* matrixColumns);
jstring toJstring
  (const char* string);
jobjectArray toJstringArray
  (const char** arrayValue, int* arrayLength);
jobjectArray toJstringMatrix
  (const char*** matrixValue, int* matrixRows, int* matrixColumns);
jobjectArray toJstringMatrixRegion
  (const char** matrixValue, int* matrixRows, int* matrixColumns);


//////////////////////////////////////////////////////
//                Global variables                  //
//////////////////////////////////////////////////////

void setGlobalInt(jobject object, const char* name, int number);

int getGlobalInt(jobject object, const char* name);

void setGlobalIntArray(jobject object, const char* name, int* number, int length);

int* getGlobalIntArray(jobject object, const char* name, int* length);

void setGlobalIntMatrix(jobject object, const char* name, int** number, int rows, int columns);

int** getGlobalIntMatrix(jobject object, const char* name, int* rows, int* columns);

void setGlobalByte(jobject object, const char* name, byte number);

byte getGlobalByte(jobject object, const char* name);

void setGlobalByteArray(jobject object, const char* name, byte* number, int length);

byte* getGlobalByteArray(jobject object, const char* name, int* length);

void setGlobalByteMatrix(jobject object, const char* name, byte** number, int rows, int columns);

byte** getGlobalByteMatrix(jobject object, const char* name, int* rows, int* columns);

void setGlobalShort(jobject object, const char* name, short number);

short getGlobalShort(jobject object, const char* name);

void setGlobalShortArray(jobject object, const char* name, short* number, int length);

short* getGlobalShortArray(jobject object, const char* name, int* length);

void setGlobalShortMatrix(jobject object, const char* name, short** number, int rows, int columns);

short** getGlobalShortMatrix(jobject object, const char* name, int* rows, int* columns);

void setGlobalLong(jobject object, const char* name, long number);

long getGlobalLong(jobject object, const char* name);

void setGlobalLongArray(jobject object, const char* name, long* number, int length);

long* getGlobalLongArray(jobject object, const char* name, int* length);

void setGlobalLongMatrix(jobject object, const char* name, long** number, int rows, int columns);

long** getGlobalLongMatrix(jobject object, const char* name, int* rows, int* columns);

void setGlobalDouble(jobject object, const char* name, double number);

double getGlobalDouble(jobject object, const char* name);

void setGlobalDoubleArray(jobject object, const char* name, double* number, int length);

double* getGlobalDoubleArray(jobject object, const char* name, int* length);

void setGlobalDoubleMatrix(jobject object, const char* name, double** number, int rows, int columns);

double** getGlobalDoubleMatrix(jobject object, const char* name, int* rows, int* columns);

void setGlobalFloat(jobject object, const char* name, float number);

float getGlobalFloat(jobject object, const char* name);

void setGlobalFloatArray(jobject object, const char* name, float* number, int length);

float* getGlobalFloatArray(jobject object, const char* name, int* length);

void setGlobalFloatMatrix(jobject object, const char* name, float** number, int rows, int columns);

float** getGlobalFloatMatrix(jobject object, const char* name, int* rows, int* columns);

void setGlobalBool(jobject object, const char* name, bool number);

bool getGlobalBool(jobject object, const char* name);

void setGlobalBoolArray(jobject object, const char* name, bool* number, int length);

bool* getGlobalBoolArray(jobject object, const char* name, int* length);

void setGlobalBoolMatrix(jobject object, const char* name, bool** number, int rows, int columns);

bool** getGlobalBoolMatrix(jobject object, const char* name, int* rows, int* columns);

void setGlobalChar(jobject object, const char* name, char number);

char getGlobalChar(jobject object, const char* name);

void setGlobalCharArray(jobject object, const char* name, char* number, int length);

char* getGlobalCharArray(jobject object, const char* name, int* length);

void setGlobalCharMatrix(jobject object, const char* name, char** number, int rows, int columns);

char** getGlobalCharMatrix(jobject object, const char* name, int* rows, int* columns);

void setGlobalString(jobject object, const char* name, const char* number);

const char* getGlobalString(jobject object, const char* name);

void setGlobalStringArray(jobject object, const char* name, const char** number, int length);

const char** getGlobalStringArray(jobject object, const char* name, int* length);

void setGlobalStringMatrix(jobject object, const char* name, const char*** number, int rows, int columns);

const char*** getGlobalStringMatrix(jobject object, const char* name, int* rows, int* columns);

#include "jni_inout.c"
#endif
